﻿namespace CardTronicsElevator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ElevatorGroupBox = new System.Windows.Forms.GroupBox();
            this.elevatorHidden = new System.Windows.Forms.PictureBox();
            this.goingDown = new System.Windows.Forms.Button();
            this.firstFloorDoor = new System.Windows.Forms.PictureBox();
            this.infoPanelFloor1 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.goingUp = new System.Windows.Forms.Button();
            this.groundFloorDoor = new System.Windows.Forms.PictureBox();
            this.groundFloorInfoBox = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.goingUpPressed = new System.Windows.Forms.Timer(this.components);
            this.onGroundFloorDoorsOpen = new System.Windows.Forms.Timer(this.components);
            this.internalPanel = new System.Windows.Forms.GroupBox();
            this.panelInfo = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1PressedTimer = new System.Windows.Forms.Timer(this.components);
            this.elevatorMovingUpTimer = new System.Windows.Forms.Timer(this.components);
            this.goingDownBtnTimer = new System.Windows.Forms.Timer(this.components);
            this.openGroundFloorDoors = new System.Windows.Forms.Timer(this.components);
            this.closeGroundFloorDoors = new System.Windows.Forms.Timer(this.components);
            this.openFirstFloorDoors = new System.Windows.Forms.Timer(this.components);
            this.closeFirstFloorDoors = new System.Windows.Forms.Timer(this.components);
            this.ElevatorGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.elevatorHidden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstFloorDoor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.infoPanelFloor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groundFloorDoor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groundFloorInfoBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.internalPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // ElevatorGroupBox
            // 
            this.ElevatorGroupBox.Controls.Add(this.elevatorHidden);
            this.ElevatorGroupBox.Controls.Add(this.goingDown);
            this.ElevatorGroupBox.Controls.Add(this.firstFloorDoor);
            this.ElevatorGroupBox.Controls.Add(this.infoPanelFloor1);
            this.ElevatorGroupBox.Controls.Add(this.pictureBox5);
            this.ElevatorGroupBox.Controls.Add(this.goingUp);
            this.ElevatorGroupBox.Controls.Add(this.groundFloorDoor);
            this.ElevatorGroupBox.Controls.Add(this.groundFloorInfoBox);
            this.ElevatorGroupBox.Controls.Add(this.pictureBox1);
            this.ElevatorGroupBox.Location = new System.Drawing.Point(12, 12);
            this.ElevatorGroupBox.Name = "ElevatorGroupBox";
            this.ElevatorGroupBox.Size = new System.Drawing.Size(375, 639);
            this.ElevatorGroupBox.TabIndex = 0;
            this.ElevatorGroupBox.TabStop = false;
            this.ElevatorGroupBox.Text = "Elevator Group Box";
            // 
            // elevatorHidden
            // 
            this.elevatorHidden.Location = new System.Drawing.Point(43, 452);
            this.elevatorHidden.Name = "elevatorHidden";
            this.elevatorHidden.Size = new System.Drawing.Size(100, 50);
            this.elevatorHidden.TabIndex = 2;
            this.elevatorHidden.TabStop = false;
            this.elevatorHidden.Visible = false;
            // 
            // goingDown
            // 
            this.goingDown.BackgroundImage = global::CardTronicsElevator.Properties.Resources.down2;
            this.goingDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.goingDown.Location = new System.Drawing.Point(213, 162);
            this.goingDown.Name = "goingDown";
            this.goingDown.Size = new System.Drawing.Size(38, 38);
            this.goingDown.TabIndex = 7;
            this.goingDown.UseVisualStyleBackColor = true;
            this.goingDown.Click += new System.EventHandler(this.goingDownPressed);
            // 
            // firstFloorDoor
            // 
            this.firstFloorDoor.BackgroundImage = global::CardTronicsElevator.Properties.Resources.door;
            this.firstFloorDoor.Location = new System.Drawing.Point(22, 71);
            this.firstFloorDoor.Name = "firstFloorDoor";
            this.firstFloorDoor.Size = new System.Drawing.Size(157, 244);
            this.firstFloorDoor.TabIndex = 6;
            this.firstFloorDoor.TabStop = false;
            // 
            // infoPanelFloor1
            // 
            this.infoPanelFloor1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.infoPanelFloor1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.infoPanelFloor1.Image = global::CardTronicsElevator.Properties.Resources.basic;
            this.infoPanelFloor1.Location = new System.Drawing.Point(87, 42);
            this.infoPanelFloor1.Name = "infoPanelFloor1";
            this.infoPanelFloor1.Size = new System.Drawing.Size(22, 13);
            this.infoPanelFloor1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.infoPanelFloor1.TabIndex = 4;
            this.infoPanelFloor1.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::CardTronicsElevator.Properties.Resources.openElevator1;
            this.pictureBox5.Location = new System.Drawing.Point(6, 34);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(190, 296);
            this.pictureBox5.TabIndex = 5;
            this.pictureBox5.TabStop = false;
            // 
            // goingUp
            // 
            this.goingUp.BackgroundImage = global::CardTronicsElevator.Properties.Resources.up;
            this.goingUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.goingUp.Location = new System.Drawing.Point(213, 464);
            this.goingUp.Name = "goingUp";
            this.goingUp.Size = new System.Drawing.Size(38, 38);
            this.goingUp.TabIndex = 3;
            this.goingUp.UseVisualStyleBackColor = true;
            this.goingUp.Click += new System.EventHandler(this.goingUpBtnPressed);
            // 
            // groundFloorDoor
            // 
            this.groundFloorDoor.BackgroundImage = global::CardTronicsElevator.Properties.Resources.door;
            this.groundFloorDoor.Location = new System.Drawing.Point(22, 373);
            this.groundFloorDoor.Name = "groundFloorDoor";
            this.groundFloorDoor.Size = new System.Drawing.Size(157, 244);
            this.groundFloorDoor.TabIndex = 2;
            this.groundFloorDoor.TabStop = false;
            // 
            // groundFloorInfoBox
            // 
            this.groundFloorInfoBox.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groundFloorInfoBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groundFloorInfoBox.Image = global::CardTronicsElevator.Properties.Resources.basic;
            this.groundFloorInfoBox.Location = new System.Drawing.Point(87, 344);
            this.groundFloorInfoBox.Name = "groundFloorInfoBox";
            this.groundFloorInfoBox.Size = new System.Drawing.Size(22, 13);
            this.groundFloorInfoBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.groundFloorInfoBox.TabIndex = 1;
            this.groundFloorInfoBox.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::CardTronicsElevator.Properties.Resources.openElevator1;
            this.pictureBox1.Location = new System.Drawing.Point(6, 336);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(190, 296);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // onGroundFloorDoorsOpen
            // 
            this.onGroundFloorDoorsOpen.Interval = 3;
            // 
            // internalPanel
            // 
            this.internalPanel.Controls.Add(this.panelInfo);
            this.internalPanel.Controls.Add(this.pictureBox2);
            this.internalPanel.Location = new System.Drawing.Point(393, 12);
            this.internalPanel.Name = "internalPanel";
            this.internalPanel.Size = new System.Drawing.Size(200, 532);
            this.internalPanel.TabIndex = 1;
            this.internalPanel.TabStop = false;
            this.internalPanel.Text = "ControlPanel-Futureimprovment";
            // 
            // panelInfo
            // 
            this.panelInfo.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panelInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelInfo.Location = new System.Drawing.Point(49, 42);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(105, 91);
            this.panelInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.panelInfo.TabIndex = 2;
            this.panelInfo.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::CardTronicsElevator.Properties.Resources.panel1;
            this.pictureBox2.Location = new System.Drawing.Point(2, 19);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(197, 518);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // elevatorMovingUpTimer
            // 
            this.elevatorMovingUpTimer.Interval = 20;
            this.elevatorMovingUpTimer.Tick += new System.EventHandler(this.movingDownBtn);
            // 
            // goingDownBtnTimer
            // 
            this.goingDownBtnTimer.Interval = 8;
            this.goingDownBtnTimer.Tick += new System.EventHandler(this.movingUpBtn);
            // 
            // openGroundFloorDoors
            // 
            this.openGroundFloorDoors.Interval = 8;
            this.openGroundFloorDoors.Tick += new System.EventHandler(this.openGroundFloorDoor);
            // 
            // closeGroundFloorDoors
            // 
            this.closeGroundFloorDoors.Interval = 8;
            this.closeGroundFloorDoors.Tick += new System.EventHandler(this.closeGroundFloorDoor);
            // 
            // openFirstFloorDoors
            // 
            this.openFirstFloorDoors.Interval = 8;
            this.openFirstFloorDoors.Tick += new System.EventHandler(this.openFirstFloorDoor);
            // 
            // closeFirstFloorDoors
            // 
            this.closeFirstFloorDoors.Interval = 8;
            this.closeFirstFloorDoors.Tick += new System.EventHandler(this.closeFirstFloorDoor);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CardTronicsElevator.Properties.Resources.background_01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 740);
            this.Controls.Add(this.internalPanel);
            this.Controls.Add(this.ElevatorGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "MainForm";
            this.Opacity = 0.95D;
            this.Text = "Elevator";
            this.ElevatorGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.elevatorHidden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstFloorDoor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.infoPanelFloor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groundFloorDoor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groundFloorInfoBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.internalPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox ElevatorGroupBox;
        private System.Windows.Forms.Button goingUp;
        private System.Windows.Forms.PictureBox groundFloorDoor;
        private System.Windows.Forms.PictureBox groundFloorInfoBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer goingUpPressed;
        private System.Windows.Forms.Timer onGroundFloorDoorsOpen;
        private System.Windows.Forms.GroupBox internalPanel;
        private System.Windows.Forms.PictureBox panelInfo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Timer panel1PressedTimer;
        private System.Windows.Forms.Button goingDown;
        private System.Windows.Forms.PictureBox firstFloorDoor;
        private System.Windows.Forms.PictureBox infoPanelFloor1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox elevatorHidden;
        private System.Windows.Forms.Timer elevatorMovingUpTimer;
        private System.Windows.Forms.Timer goingDownBtnTimer;
        private System.Windows.Forms.Timer openGroundFloorDoors;
        private System.Windows.Forms.Timer closeGroundFloorDoors;
        private System.Windows.Forms.Timer openFirstFloorDoors;
        private System.Windows.Forms.Timer closeFirstFloorDoors;
    }
}

