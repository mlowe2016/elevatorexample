﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardTronicsElevator.Config
{
    class Configuration
    {
        //To-Do imporft from configuration file

        public int x_open = -110;

        public int x_close = 21;

        protected int y_up = 63;

        protected int y_down = 376;

        protected int elevatorSpeed = 2;

        protected System.Windows.Forms.PictureBox localPicRef { get; set; }

        protected bool animationFinished { get; set; } = false;


    }
}
