﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CardTronicsElevator.Config;

namespace CardTronicsElevator.Animation.Class
{

    class OpenCloseDoor : Configuration
    {
       
        public OpenCloseDoor(System.Windows.Forms.PictureBox pbx)
        {
            localPicRef = pbx;
            
        }
        public bool GetAnimationStatus()
        {
            return this.animationFinished;
        }

        public void OpenDoorAnimation()
        {
            animationFinished = false ;
            if (localPicRef.Left >= x_open)
            {
                localPicRef.Left -= 1;
            }
            else
            {
                animationFinished = true;
            }
        }

        public void CloseDoorAnimation()
        {
            animationFinished = false;
            
            if (localPicRef.Left <= x_close)
            {
                localPicRef.Left += 1;
            }
            else
            {
                animationFinished = true;
            }
        }
    }
}
