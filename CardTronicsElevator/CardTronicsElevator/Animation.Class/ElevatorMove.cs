﻿using System;
using System.Collections.Generic;
using System.Text;
using CardTronicsElevator.Config;

namespace CardTronicsElevator.Animation.Class
{
    class ElevatorMove : Configuration
    {
        public ElevatorMove(System.Windows.Forms.PictureBox pbx)
        {
            localPicRef = pbx;
        }
        public bool GetAnimationStatus()
        {
            return this.animationFinished;
        }
        public void MoveUpAnimation()
        {
            if (localPicRef.Top >= y_up)
            {
                localPicRef.Top -= elevatorSpeed;
            }
            else
            {
                animationFinished = true;
            }
        }

        public void MoveDownAnimation()
        {
            if (localPicRef.Top <= y_down)
            {
                localPicRef.Top += elevatorSpeed;
            }
            else
            {
                animationFinished = true;
            }
        }
    }
}
