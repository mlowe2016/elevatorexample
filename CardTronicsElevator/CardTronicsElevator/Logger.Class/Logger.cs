﻿using System;
using System.IO;
using System.Threading;

namespace CardTronicsElevator.Logger
{
    public sealed class Logger
    {
        private static readonly Logger _logger = new Logger();

        // TODO further work can be done here to configure the paths of the log
        private string logFolder = "C:\\Users\\Public\\";

        public string logName = "elevatorLog.log";

        private static ReaderWriterLock _readerWriteLock = new ReaderWriterLock();

        public static Logger GetLogger()
        {
            return _logger;
        }

        public bool StartLog()
        {
            try
            {
                WriteMessage("**SYSTEM START****************************************************************************");
            }
            catch
            {
                //To-do add exception handle log
                throw new System.IO.IOException("Error when staring the log");

            }

            return true;
        }

        public bool WriteMessage(string message)
        {
            /* lock the file */
            _readerWriteLock.AcquireReaderLock(Timeout.Infinite);
            try
            {
                using (StreamWriter w = File.AppendText(logFolder + logName))
                {
                    var messageNew = DateTime.Now + ": " + message;
                    w.WriteLine(messageNew);
                }
            }
            catch
            {
                return false;
            }
            finally
            {
                _readerWriteLock.ReleaseReaderLock();
            }

            return true;
        }
    }

    }
