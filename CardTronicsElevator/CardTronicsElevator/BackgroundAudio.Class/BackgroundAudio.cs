﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Media;
using CardTronicsElevator.Logger;

namespace CardTronicsElevator.BackgroundAudio.Class
{
    class BackgroundAudio
    {
        private SoundPlayer Player = new SoundPlayer(Properties.Resources.EM1);

        public BackgroundAudio()
        {
            try
            {
                // Note: You may need to change the location specified based on
                // the sounds loaded on your computer.
                this.Player.PlayLooping();
            }
            catch (Exception ex)
            {
                Logger.Logger.GetLogger().WriteMessage(ex.Message + " Error playing sound " + this.Player.SoundLocation);
            }
        }
    }
}
