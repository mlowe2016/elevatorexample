﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CardTronicsElevator.Animation.Class;
using CardTronicsElevator.BackgroundAudio.Class;

namespace CardTronicsElevator
{
    public partial class MainForm : Form
    {
        public string currentFloor = "Ground Floor";

        public bool moving = false;

        public MainForm()
        {
            InitializeComponent();
            //Starting the log as its a singleton object
            Logger.Logger.GetLogger().StartLog();
            //TO-DO future work when starting have the panel disabled until the door is opened
            DisablePanel();
            //Background song - Suggest moving to the MediaPLayer class as this will allow pause, stop etc
            var backgroundSong = new BackgroundAudio.Class.BackgroundAudio();
        }

        #region Utilities 

        /// <summary>
        /// TO-DO - disable a panel of options when  in movement
        /// </summary>
        
        public void DisablePanel()
        {
           //TO-DO future work disable the panel in movement
        }
        
        /// <summary>
        /// TO-DO - enable a panel of options when not in movement
        /// </summary>
        
        public void EnablePanel()
        {
            //TO-DO future work enable the panel when not moving
        }

       /// <summary>
       /// Utility Class that should really be added to a static class of utilites
       /// The usage here is to update all the panels on the screen at a time
       /// </summary>
       
        private void SetInfoPanels(Bitmap bm)
        {

            groundFloorInfoBox.Image = bm;
            panelInfo.Image = bm;
            infoPanelFloor1.Image = bm;
        }

        /// <summary>
        /// Utility that should really be added to a static class of utilites
        /// if the elevator is moving we do not want to set the panel ... yet
        /// </summary>
        /// 
        private void IsElevatorMovingPanelUpdate(string state)
        {
            if (!moving)
            {
                switch (state)
                {
                    case "Ground Floor":
                    SetInfoPanels(global::CardTronicsElevator.Properties.Resources.G);
                        break;
                    case "First Floor":
                        SetInfoPanels(global::CardTronicsElevator.Properties.Resources._1);
                        break;
                }
            }
        }

      
        #endregion

        #region DoorControls

        /// <summary>
        /// Button Event that handles the downward push of a button
        /// Future improvment would be to use an instance of the switch state class as suggested in the created classes
        /// </summary>

        private void goingDownPressed(object sender, EventArgs e)
        {
            //Logging to the file
            Logger.Logger.GetLogger().WriteMessage("Button press:Going down");

            //if we are on the ground floor
            if (currentFloor == "Ground Floor")
            {
                //Log to log file
                Logger.Logger.GetLogger().WriteMessage("Status changing:Moving to first floor");
                //as we are moving set the info panel images to the up arrow
                SetInfoPanels(global::CardTronicsElevator.Properties.Resources.up_arrow);
                //enable the down movement of the elevator
                goingDownBtnTimer.Enabled = true;
                //stop the existing animation from running
                openFirstFloorDoors.Enabled = false;
            }
            else
            {
                //Log to log file
                Logger.Logger.GetLogger().WriteMessage("Status unchanged:Opening door");
                //stop the existing animation from running
                goingDownBtnTimer.Enabled = false;
                //Open the doors
                openFirstFloorDoors.Enabled = true;

            }
        }


        /// <summary>
        /// Button Event that handles the upward push of a button
        /// Future improvment would be to use an instance of the switch state class as suggested in the created classes
        /// </summary>
        
        private void goingUpBtnPressed(object sender, EventArgs e)
        {
            //Log to log file
            Logger.Logger.GetLogger().WriteMessage("Button press:Going up");
            //if we are on the ground floor
            if (currentFloor == "Ground Floor")
            {
                //Log to log file
                Logger.Logger.GetLogger().WriteMessage("Status unchanged:Opening door");
                //open the ground floor doors
                openGroundFloorDoors.Enabled = true;
            }
            else
            {
                //move the elevator 
                Logger.Logger.GetLogger().WriteMessage("Status changing:Moving to ground floor");
                //as we are moving down set the image to the downward arrow GIf
                SetInfoPanels(global::CardTronicsElevator.Properties.Resources.down_arrow);
                //close the open first floor door with animation timer
                closeFirstFloorDoors.Enabled = true;
                //move the elevator
                elevatorMovingUpTimer.Enabled = true;

            }
        }
        #endregion

        #region Tickers

        /// <summary>
        /// Handler for the move up button which is linked to a
        /// </summary>
        
        private void movingUpBtn(object sender, EventArgs e)
        {
            //Log to file
            Logger.Logger.GetLogger().WriteMessage("Animation:Moving Up");
            //Disable the panel
            DisablePanel();
            //Colour the button
            goingDown.BackColor = Color.Green;
            //local instance of the animation
            var moveElevatorUp = new ElevatorMove(this.elevatorHidden);
            //close doors
            closeGroundFloorDoors.Enabled = true;
            //animate the move animation
            moveElevatorUp.MoveUpAnimation();
            //set elevator to moving
            moving = true;
            //if the current animation has stopped/completed
            if (moveElevatorUp.GetAnimationStatus())
            {
                //elevator has stopped
                moving = false;
                //reset the colour
                goingDown.BackColor = Color.White;
                //Set the current floor
                currentFloor = "Frist Floor";
                //Write to the logger as this will show a change
                Logger.Logger.GetLogger().WriteMessage("Setting current floor " + currentFloor);
                //animation to open the first floor doors
                openFirstFloorDoors.Enabled = true;
                //animation for the downward movement stopped
                goingDownBtnTimer.Enabled = false;

            }
        }
        /// <summary>
        /// Handler for the move down button which is linked to a
        /// </summary>
        private void movingDownBtn(object sender, EventArgs e)
        {
            //Write to logger
            Logger.Logger.GetLogger().WriteMessage("Animation:Moving Down");
            //stop the open doors animation to avoid clashing
            openFirstFloorDoors.Enabled = false;
            //enable close door animation
            closeFirstFloorDoors.Enabled = true;
            //Disbale the control panel while moving
            DisablePanel();
            //Set background colour to green
            goingUp.BackColor = Color.Green;
            //local instance of the elevator moving
            var moveElevatorDown = new ElevatorMove(this.elevatorHidden);
            //move the elevator down animation
            moveElevatorDown.MoveDownAnimation();
            //configre to be moving
            moving = true;
            //if the animation has stopped
            if (moveElevatorDown.GetAnimationStatus())
            {
                //the system is no longer moving
                moving = false;
                //setting the current floor
                currentFloor = "Ground Floor";
                //Reset the background colour
                goingUp.BackColor = Color.White;
                //Write to log of the current floor level
                Logger.Logger.GetLogger().WriteMessage("Setting current floor " + currentFloor);
                //stop the animation moving up
                elevatorMovingUpTimer.Enabled = false;
                //stop the timer animation for closing the doors
                closeFirstFloorDoors.Enabled = false;
                //Start timer for the open animation on the ground floor
                openGroundFloorDoors.Enabled = true;
            }
        }
        /// <summary>
        /// Handler for the opening of the ground floor doors
        /// </summary>
        private void openGroundFloorDoor(object sender, EventArgs e)
        {
            //write to logger
            Logger.Logger.GetLogger().WriteMessage("Animation:Opening ground floor door");
            //set the background colour
            goingUp.BackColor = Color.Green;
            //local instanceof the open animation
            var gfDoor = new OpenCloseDoor(this.groundFloorDoor);
            //enable the animation of the doors
            gfDoor.OpenDoorAnimation();
            //If the animation has finished
            if (gfDoor.GetAnimationStatus())
            {
              
            IsElevatorMovingPanelUpdate(currentFloor);

                //disbale the animation timer for opening the ground floor doors
                openGroundFloorDoors.Enabled = false;
                //Reset the colour back to normal
                goingUp.BackColor = Color.White;
            }
        }
        /// <summary>
        /// Handler for the closing of the ground floor doors
        /// </summary>
        private void closeGroundFloorDoor(object sender, EventArgs e)
        {
            //Writting to the log
            Logger.Logger.GetLogger().WriteMessage("Animation:Closing ground floor door");
            //Set the button background to green for user experience
            goingUp.BackColor = Color.Green;
            //local instance of the openclose door in this case it will be used for the ground floor
            var gfDoor = new OpenCloseDoor(this.groundFloorDoor);
            //Animate the closing of the door
            gfDoor.CloseDoorAnimation();
            //if the animation has finished
            if (gfDoor.GetAnimationStatus())
            {
                //if the elevator is not moving
                IsElevatorMovingPanelUpdate(currentFloor);
                //disable the close door timer animation
                closeGroundFloorDoors.Enabled = false;
                //Reset the colour of the button back to normal
                goingUp.BackColor = Color.White;
            }
        }
        /// <summary>
        /// Handler for the opening of the firstfloor door
        /// </summary>
        private void openFirstFloorDoor(object sender, EventArgs e)
        {
            //Log to file
            Logger.Logger.GetLogger().WriteMessage("Animation:Opening first floor door");
            //Set the colour of the button to green
            goingDown.BackColor = Color.Green;
            //local instance of the animation
            var ffDoor = new OpenCloseDoor(this.firstFloorDoor);
            //Start the animation
            ffDoor.OpenDoorAnimation();
            // if the animation has finished
            if (ffDoor.GetAnimationStatus())
            {
                //If not moving then set to 1
                IsElevatorMovingPanelUpdate(currentFloor);
                //disable animation
                openFirstFloorDoors.Enabled = false;
                //reset color back to white
                goingDown.BackColor = Color.White;
            }
        }
        /// <summary>
        /// Handler for the closing of the firstfloor door
        /// </summary>
        private void closeFirstFloorDoor(object sender, EventArgs e)
        {
            //Write to log file
            Logger.Logger.GetLogger().WriteMessage("Animation:Closing first floor door");
            //Set background colour to green
            goingDown.BackColor = Color.Green;
            //localk instance of door animation
            var ffDoor = new OpenCloseDoor(this.firstFloorDoor);
            //Start the animation
            ffDoor.CloseDoorAnimation();
            //if the animation has finished
            if (ffDoor.GetAnimationStatus())
            {
                //if the elevator is not moving
                IsElevatorMovingPanelUpdate(currentFloor);
                //Disable the timer animation for the doors
                closeFirstFloorDoors.Enabled = false;
                //reset the colour back to normal white
                goingDown.BackColor = Color.White;
            }
        }


        #endregion

    }   
}
