﻿using System;
using System.Collections.Generic;
using System.Text;
using CardTronicsElevator.Elevator.Class;

namespace CardTronicsElevator.Floor.Class
{
    class Floor
    {
        private int numberOfPeople {get; set;}
        private int floorNumber { get; set; }
        private bool isCurrentFloor { get; set; }
        private int numberOfElevators { get; set; }
        private ElevatorObj floorElevator { get; set; }
    }
}
