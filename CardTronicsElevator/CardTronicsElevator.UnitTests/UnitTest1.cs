﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CardTronicsElevator.Logger;

namespace CardTronicsElevator.Test
{
    [TestClass]
    public class LoggerTests
    {
        private string UnitTestLogName = "C:\\Users\\Public\\unitTesting.log";

        [TestMethod]
        public void LoggerWriteMessage_StartLog_SucceedsWrite()
        {
            Logger.Logger.GetLogger().WriteMessage(UnitTestLogName);
            Logger.Logger.GetLogger().logName = UnitTestLogName;
            // Act
            var result = Logger.Logger.GetLogger().StartLog();
            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void LoggerWriteMessage_WriteMessage_EmptySucceedsWrite()
        {
            Logger.Logger.GetLogger().logName = UnitTestLogName;
            // Act
            var result = Logger.Logger.GetLogger().WriteMessage("");
            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void LoggerWriteMessage_WriteMessage_NullSucceedsWrite()
        {
            Logger.Logger.GetLogger().logName = UnitTestLogName;
            // Act
            var result = Logger.Logger.GetLogger().WriteMessage(null);
            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void LoggerWriteMessage_WriteMessage_TestStringSucceedsWrite()
        {
            Logger.Logger.GetLogger().logName = UnitTestLogName;
            // Act
            var result = Logger.Logger.GetLogger().WriteMessage("I_AM_A_TEST");
            // Assert
            Assert.IsTrue(result);
        }
    }
}
